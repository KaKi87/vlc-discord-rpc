local discordRpc = require "lua-discordRPC.discordRPC"

function descriptor()
    local name = "Discord Rich Presence"
    return {
        title = name,
        shortdesc = name,
        description = "Discord rich presence integration for VLC Media Player",
        version = "1.0.0",
        author = "KaKi87 & dylhack",
        url = "https://git.kaki87.net/KaKi87/vlc-discord-rpc",
        capabilities = { "menu", "playing-listener", "input-listener" }
    }
end

function activate()
    -- Triggered when the plugin is enabled
    -- TODO connect RPC
end

function deactivate()
    -- Triggered when the plugin is disabled
    -- TODO disconnect RPC
end

function playing_changed()
    -- Triggered when play/pause/stop
    local status = vlc.playlist.status() -- playing/paused/stopped
    local file = vlc.input.item():name() -- media file name
    -- TODO add elapsed time + remaining time
    -- TODO update RPC
end